// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33233,y:32739,varname:node_3138,prsc:2|emission-3772-RGB,clip-6988-OUT;n:type:ShaderForge.SFN_Tex2d,id:5441,x:32171,y:32981,ptovrint:False,ptlb:Wipe Texture,ptin:_WipeTexture,varname:node_6540,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:9216,x:32068,y:33243,ptovrint:False,ptlb:Wipe,ptin:_Wipe,varname:node_9905,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:107,x:32337,y:33173,varname:node_107,prsc:2|A-5441-R,B-9216-OUT;n:type:ShaderForge.SFN_Vector1,id:5223,x:32586,y:33147,varname:node_5223,prsc:2,v1:-0.5;n:type:ShaderForge.SFN_Add,id:6988,x:32836,y:33180,varname:node_6988,prsc:2|A-5223-OUT,B-107-OUT;n:type:ShaderForge.SFN_Tex2d,id:3772,x:32996,y:32827,ptovrint:False,ptlb:Base Texture,ptin:_BaseTexture,varname:node_5784,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-4654-UVOUT;n:type:ShaderForge.SFN_Panner,id:4654,x:32446,y:32774,varname:node_4654,prsc:2,spu:1,spv:1|UVIN-917-UVOUT,DIST-3241-OUT;n:type:ShaderForge.SFN_TexCoord,id:917,x:31732,y:32723,varname:node_917,prsc:2,uv:0;n:type:ShaderForge.SFN_Time,id:7813,x:31716,y:33034,varname:node_7813,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3241,x:32125,y:32609,varname:node_3241,prsc:2|A-7813-T,B-3120-OUT;n:type:ShaderForge.SFN_Vector1,id:3120,x:31914,y:32544,varname:node_3120,prsc:2,v1:0.02;proporder:3772-5441-9216;pass:END;sub:END;*/

Shader "Shader Forge/shader_ui_screenwipe_scroll" {
    Properties {
        _BaseTexture ("Base Texture", 2D) = "white" {}
        _WipeTexture ("Wipe Texture", 2D) = "white" {}
        _Wipe ("Wipe", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _WipeTexture; uniform float4 _WipeTexture_ST;
            uniform float _Wipe;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _WipeTexture_var = tex2D(_WipeTexture,TRANSFORM_TEX(i.uv0, _WipeTexture));
                clip(((-0.5)+(_WipeTexture_var.r+_Wipe)) - 0.5);
////// Lighting:
////// Emissive:
                float4 node_7813 = _Time + _TimeEditor;
                float2 node_4654 = (i.uv0+(node_7813.g*0.02)*float2(1,1));
                float4 _BaseTexture_var = tex2D(_BaseTexture,TRANSFORM_TEX(node_4654, _BaseTexture));
                float3 emissive = _BaseTexture_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _WipeTexture; uniform float4 _WipeTexture_ST;
            uniform float _Wipe;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _WipeTexture_var = tex2D(_WipeTexture,TRANSFORM_TEX(i.uv0, _WipeTexture));
                clip(((-0.5)+(_WipeTexture_var.r+_Wipe)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
