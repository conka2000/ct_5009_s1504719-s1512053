// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:-1000,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:4795,x:32716,y:32678,varname:node_4795,prsc:2|emission-102-RGB,clip-4794-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:31913,y:32829,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:9112,x:32299,y:33009,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_9112,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:d70cd5f757762fe42b3c21eb1e665096,ntxv:0,isnm:False|UVIN-1225-UVOUT;n:type:ShaderForge.SFN_Multiply,id:4794,x:32511,y:32964,varname:node_4794,prsc:2|A-2053-A,B-9112-R;n:type:ShaderForge.SFN_Panner,id:1225,x:32099,y:33009,varname:node_1225,prsc:2,spu:0.5,spv:0.5|UVIN-7691-UVOUT,DIST-2053-B;n:type:ShaderForge.SFN_TexCoord,id:7691,x:31924,y:33009,varname:node_7691,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:102,x:32511,y:32755,ptovrint:False,ptlb:node_102,ptin:_node_102,varname:node_102,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e3d8ded545f4995459cbabfb4af60a53,ntxv:0,isnm:False|UVIN-8384-OUT;n:type:ShaderForge.SFN_Append,id:8384,x:32294,y:32755,varname:node_8384,prsc:2|A-2053-R,B-5468-OUT;n:type:ShaderForge.SFN_Vector1,id:5468,x:31913,y:32742,varname:node_5468,prsc:2,v1:0;proporder:9112-102;pass:END;sub:END;*/

Shader "Shader Forge/shader_particle_sphere" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _node_102 ("node_102", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            Offset -1000, 0
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform sampler2D _node_102; uniform float4 _node_102_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 node_1225 = (i.uv0+i.vertexColor.b*float2(0.5,0.5));
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(node_1225, _Texture));
                float node_4794 = (i.vertexColor.a*_Texture_var.r);
                clip(node_4794 - 0.5);
////// Lighting:
////// Emissive:
                float2 node_8384 = float2(i.vertexColor.r,0.0);
                float4 _node_102_var = tex2D(_node_102,TRANSFORM_TEX(node_8384, _node_102));
                float3 emissive = _node_102_var.rgb;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
