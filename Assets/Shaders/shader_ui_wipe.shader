// Shader created with Shader Forge v1.27 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.27;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:6,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:False,qofs:0,qpre:0,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32867,y:32707,varname:node_3138,prsc:2|emission-5784-RGB,clip-1537-OUT;n:type:ShaderForge.SFN_Tex2d,id:6540,x:31876,y:32812,ptovrint:False,ptlb:Wipe Texture,ptin:_WipeTexture,varname:node_6540,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5784,x:32326,y:32693,ptovrint:False,ptlb:Base Texture,ptin:_BaseTexture,varname:node_5784,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:1537,x:32644,y:32988,varname:node_1537,prsc:2|A-7997-OUT,B-9726-OUT;n:type:ShaderForge.SFN_Vector1,id:7997,x:32394,y:32955,varname:node_7997,prsc:2,v1:-0.5;n:type:ShaderForge.SFN_Add,id:9726,x:32145,y:32981,varname:node_9726,prsc:2|A-6540-R,B-9905-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9905,x:31876,y:33051,ptovrint:False,ptlb:Wipe,ptin:_Wipe,varname:node_9905,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:1464,x:32629,y:32512,varname:node_1464,prsc:2|A-1775-RGB,B-5784-RGB,T-5784-A;n:type:ShaderForge.SFN_Color,id:1775,x:32215,y:32227,ptovrint:False,ptlb:node_1775,ptin:_node_1775,varname:node_1775,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1882353,c2:0.2392157,c3:0.3098039,c4:1;proporder:6540-5784-9905-1775;pass:END;sub:END;*/

Shader "Shader Forge/UI_Wipe" {
    Properties {
        _WipeTexture ("Wipe Texture", 2D) = "white" {}
        _BaseTexture ("Base Texture", 2D) = "white" {}
        _Wipe ("Wipe", Float ) = 0
        _node_1775 ("node_1775", Color) = (0.1882353,0.2392157,0.3098039,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="Background"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZTest Always
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _WipeTexture; uniform float4 _WipeTexture_ST;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            uniform float _Wipe;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _WipeTexture_var = tex2D(_WipeTexture,TRANSFORM_TEX(i.uv0, _WipeTexture));
                clip(((-0.5)+(_WipeTexture_var.r+_Wipe)) - 0.5);
////// Lighting:
////// Emissive:
                float4 _BaseTexture_var = tex2D(_BaseTexture,TRANSFORM_TEX(i.uv0, _BaseTexture));
                float3 emissive = _BaseTexture_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _WipeTexture; uniform float4 _WipeTexture_ST;
            uniform float _Wipe;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _WipeTexture_var = tex2D(_WipeTexture,TRANSFORM_TEX(i.uv0, _WipeTexture));
                clip(((-0.5)+(_WipeTexture_var.r+_Wipe)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
