// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.27 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.27;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:34578,y:32699,varname:node_4013,prsc:2|diff-3572-OUT;n:type:ShaderForge.SFN_Multiply,id:7527,x:32766,y:32979,varname:node_7527,prsc:2|A-1550-A,B-5000-G;n:type:ShaderForge.SFN_Posterize,id:2853,x:32972,y:32979,varname:node_2853,prsc:2|IN-7527-OUT,STPS-4853-OUT;n:type:ShaderForge.SFN_Tex2d,id:6215,x:33379,y:32565,ptovrint:False,ptlb:Base texture,ptin:_Basetexture,varname:node_6215,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:485d987b2ba780a4c9ba1110c70a2ebc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7358,x:33379,y:32748,ptovrint:False,ptlb:Grass texture (red),ptin:_Grasstexturered,varname:node_7358,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4c21ec345876a5649b42bceff21fe19d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:2306,x:33567,y:32609,varname:node_2306,prsc:2|A-6215-RGB,B-3682-RGB,T-9446-OUT;n:type:ShaderForge.SFN_Tex2d,id:5000,x:32393,y:33028,varname:node_5000,prsc:2,ntxv:0,isnm:False|TEX-6485-TEX;n:type:ShaderForge.SFN_Multiply,id:7220,x:33166,y:32959,varname:node_7220,prsc:2|A-9419-OUT,B-2853-OUT;n:type:ShaderForge.SFN_Vector1,id:9419,x:32972,y:32922,varname:node_9419,prsc:2,v1:2;n:type:ShaderForge.SFN_Smoothstep,id:9446,x:33379,y:32918,varname:node_9446,prsc:2|A-2378-OUT,B-4729-OUT,V-7220-OUT;n:type:ShaderForge.SFN_Vector1,id:2378,x:33166,y:32838,varname:node_2378,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:4729,x:33166,y:32898,varname:node_4729,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:4853,x:32766,y:33107,varname:node_4853,prsc:2,v1:8;n:type:ShaderForge.SFN_Tex2d,id:3682,x:33379,y:32372,ptovrint:False,ptlb:Rock texture (green),ptin:_Rocktexturegreen,varname:node_3682,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9f1959739d184a844ad4f56c6469e134,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:3572,x:33795,y:32609,varname:node_3572,prsc:2|A-2306-OUT,B-7358-RGB,T-228-OUT;n:type:ShaderForge.SFN_Multiply,id:3428,x:32766,y:33182,varname:node_3428,prsc:2|A-1781-OUT,B-1550-A;n:type:ShaderForge.SFN_Posterize,id:23,x:32972,y:33180,varname:node_23,prsc:2|IN-3428-OUT,STPS-4853-OUT;n:type:ShaderForge.SFN_Multiply,id:4217,x:33187,y:33180,varname:node_4217,prsc:2|A-9419-OUT,B-23-OUT;n:type:ShaderForge.SFN_Smoothstep,id:228,x:33400,y:33155,varname:node_228,prsc:2|A-2378-OUT,B-4729-OUT,V-4217-OUT;n:type:ShaderForge.SFN_Tex2d,id:1550,x:32393,y:33178,varname:node_1550,prsc:2,ntxv:0,isnm:False|UVIN-2954-OUT,TEX-6485-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:6485,x:32047,y:33109,ptovrint:False,ptlb:Blend texture,ptin:_Blendtexture,varname:node_6485,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:4685,x:31820,y:33210,varname:node_4685,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:2954,x:32047,y:33282,varname:node_2954,prsc:2|A-4685-UVOUT,B-4534-OUT;n:type:ShaderForge.SFN_Vector1,id:4534,x:31820,y:33367,varname:node_4534,prsc:2,v1:10;n:type:ShaderForge.SFN_Tex2d,id:8143,x:32574,y:32559,ptovrint:False,ptlb:node_7389_copy_copy,ptin:_node_7389_copy_copy,varname:_node_7389_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5742,x:32311,y:33452,ptovrint:False,ptlb:r2t_mask,ptin:_r2t_mask,varname:node_5742,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:950151fe2d78a0047b2a5e481a2bfb3b,ntxv:0,isnm:False|UVIN-6715-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:6715,x:32124,y:33422,varname:node_6715,prsc:2,uv:1;n:type:ShaderForge.SFN_Add,id:1781,x:32567,y:33363,varname:node_1781,prsc:2|A-5000-R,B-5742-R;proporder:6215-7358-3682-6485-5742;pass:END;sub:END;*/

Shader "Shader Forge/shader_groundplane_r2t_mask" {
    Properties {
        _Basetexture ("Base texture", 2D) = "white" {}
        _Grasstexturered ("Grass texture (red)", 2D) = "white" {}
        _Rocktexturegreen ("Rock texture (green)", 2D) = "white" {}
        _Blendtexture ("Blend texture", 2D) = "white" {}
        _r2t_mask ("r2t_mask", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Basetexture; uniform float4 _Basetexture_ST;
            uniform sampler2D _Grasstexturered; uniform float4 _Grasstexturered_ST;
            uniform sampler2D _Rocktexturegreen; uniform float4 _Rocktexturegreen_ST;
            uniform sampler2D _Blendtexture; uniform float4 _Blendtexture_ST;
            uniform sampler2D _r2t_mask; uniform float4 _r2t_mask_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _Basetexture_var = tex2D(_Basetexture,TRANSFORM_TEX(i.uv0, _Basetexture));
                float4 _Rocktexturegreen_var = tex2D(_Rocktexturegreen,TRANSFORM_TEX(i.uv0, _Rocktexturegreen));
                float node_2378 = 0.0;
                float node_4729 = 1.0;
                float node_9419 = 2.0;
                float2 node_2954 = (i.uv0*10.0);
                float4 node_1550 = tex2D(_Blendtexture,TRANSFORM_TEX(node_2954, _Blendtexture));
                float4 node_5000 = tex2D(_Blendtexture,TRANSFORM_TEX(i.uv0, _Blendtexture));
                float node_4853 = 8.0;
                float4 _Grasstexturered_var = tex2D(_Grasstexturered,TRANSFORM_TEX(i.uv0, _Grasstexturered));
                float4 _r2t_mask_var = tex2D(_r2t_mask,TRANSFORM_TEX(i.uv1, _r2t_mask));
                float3 diffuseColor = lerp(lerp(_Basetexture_var.rgb,_Rocktexturegreen_var.rgb,smoothstep( node_2378, node_4729, (node_9419*floor((node_1550.a*node_5000.g) * node_4853) / (node_4853 - 1)) )),_Grasstexturered_var.rgb,smoothstep( node_2378, node_4729, (node_9419*floor(((node_5000.r+_r2t_mask_var.r)*node_1550.a) * node_4853) / (node_4853 - 1)) ));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Basetexture; uniform float4 _Basetexture_ST;
            uniform sampler2D _Grasstexturered; uniform float4 _Grasstexturered_ST;
            uniform sampler2D _Rocktexturegreen; uniform float4 _Rocktexturegreen_ST;
            uniform sampler2D _Blendtexture; uniform float4 _Blendtexture_ST;
            uniform sampler2D _r2t_mask; uniform float4 _r2t_mask_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _Basetexture_var = tex2D(_Basetexture,TRANSFORM_TEX(i.uv0, _Basetexture));
                float4 _Rocktexturegreen_var = tex2D(_Rocktexturegreen,TRANSFORM_TEX(i.uv0, _Rocktexturegreen));
                float node_2378 = 0.0;
                float node_4729 = 1.0;
                float node_9419 = 2.0;
                float2 node_2954 = (i.uv0*10.0);
                float4 node_1550 = tex2D(_Blendtexture,TRANSFORM_TEX(node_2954, _Blendtexture));
                float4 node_5000 = tex2D(_Blendtexture,TRANSFORM_TEX(i.uv0, _Blendtexture));
                float node_4853 = 8.0;
                float4 _Grasstexturered_var = tex2D(_Grasstexturered,TRANSFORM_TEX(i.uv0, _Grasstexturered));
                float4 _r2t_mask_var = tex2D(_r2t_mask,TRANSFORM_TEX(i.uv1, _r2t_mask));
                float3 diffuseColor = lerp(lerp(_Basetexture_var.rgb,_Rocktexturegreen_var.rgb,smoothstep( node_2378, node_4729, (node_9419*floor((node_1550.a*node_5000.g) * node_4853) / (node_4853 - 1)) )),_Grasstexturered_var.rgb,smoothstep( node_2378, node_4729, (node_9419*floor(((node_5000.r+_r2t_mask_var.r)*node_1550.a) * node_4853) / (node_4853 - 1)) ));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
