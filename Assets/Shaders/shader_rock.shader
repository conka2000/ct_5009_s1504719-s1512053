// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:33724,y:32809,varname:node_4013,prsc:2|diff-1919-OUT;n:type:ShaderForge.SFN_Tex2d,id:1839,x:32950,y:32725,varname:node_1839,prsc:2,ntxv:0,isnm:False|UVIN-2873-OUT,TEX-5640-TEX;n:type:ShaderForge.SFN_FragmentPosition,id:3231,x:32401,y:32751,varname:node_3231,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:5609,x:32401,y:32503,prsc:2,pt:False;n:type:ShaderForge.SFN_ChannelBlend,id:6033,x:33223,y:32755,varname:node_6033,prsc:2,chbt:0|M-9194-OUT,R-1839-RGB,G-1978-RGB,B-914-RGB;n:type:ShaderForge.SFN_Multiply,id:2339,x:32678,y:32493,varname:node_2339,prsc:2|A-5609-OUT,B-5609-OUT;n:type:ShaderForge.SFN_ComponentMask,id:9194,x:32950,y:32524,varname:node_9194,prsc:2,cc1:0,cc2:1,cc3:2,cc4:-1|IN-2339-OUT;n:type:ShaderForge.SFN_ComponentMask,id:2873,x:32712,y:32625,varname:node_2873,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-3231-XYZ;n:type:ShaderForge.SFN_ComponentMask,id:1539,x:32712,y:32780,varname:node_1539,prsc:2,cc1:2,cc2:1,cc3:-1,cc4:-1|IN-3231-XYZ;n:type:ShaderForge.SFN_ComponentMask,id:7913,x:32712,y:32934,varname:node_7913,prsc:2,cc1:0,cc2:2,cc3:-1,cc4:-1|IN-3231-XYZ;n:type:ShaderForge.SFN_Tex2dAsset,id:5640,x:32401,y:32970,ptovrint:False,ptlb:Rock texture,ptin:_Rocktexture,varname:node_5640,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:914,x:32950,y:32934,varname:node_914,prsc:2,ntxv:0,isnm:False|UVIN-1539-OUT,TEX-5640-TEX;n:type:ShaderForge.SFN_VertexColor,id:1186,x:33233,y:33131,varname:node_1186,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:1978,x:32970,y:33140,ptovrint:False,ptlb:Blend Texture,ptin:_BlendTexture,varname:node_1978,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9f1959739d184a844ad4f56c6469e134,ntxv:0,isnm:False|UVIN-7913-OUT;n:type:ShaderForge.SFN_Lerp,id:1919,x:33459,y:32893,varname:node_1919,prsc:2|A-6033-OUT,B-1978-RGB,T-1186-B;proporder:5640-1978;pass:END;sub:END;*/

Shader "Shader Forge/shader_rock" {
    Properties {
        _Rocktexture ("Rock texture", 2D) = "white" {}
        _BlendTexture ("Blend Texture", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Rocktexture; uniform float4 _Rocktexture_ST;
            uniform sampler2D _BlendTexture; uniform float4 _BlendTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 node_9194 = (i.normalDir*i.normalDir).rgb;
                float2 node_2873 = i.posWorld.rgb.rg;
                float4 node_1839 = tex2D(_Rocktexture,TRANSFORM_TEX(node_2873, _Rocktexture));
                float2 node_7913 = i.posWorld.rgb.rb;
                float4 _BlendTexture_var = tex2D(_BlendTexture,TRANSFORM_TEX(node_7913, _BlendTexture));
                float2 node_1539 = i.posWorld.rgb.bg;
                float4 node_914 = tex2D(_Rocktexture,TRANSFORM_TEX(node_1539, _Rocktexture));
                float3 diffuseColor = lerp((node_9194.r*node_1839.rgb + node_9194.g*_BlendTexture_var.rgb + node_9194.b*node_914.rgb),_BlendTexture_var.rgb,i.vertexColor.b);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Rocktexture; uniform float4 _Rocktexture_ST;
            uniform sampler2D _BlendTexture; uniform float4 _BlendTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 node_9194 = (i.normalDir*i.normalDir).rgb;
                float2 node_2873 = i.posWorld.rgb.rg;
                float4 node_1839 = tex2D(_Rocktexture,TRANSFORM_TEX(node_2873, _Rocktexture));
                float2 node_7913 = i.posWorld.rgb.rb;
                float4 _BlendTexture_var = tex2D(_BlendTexture,TRANSFORM_TEX(node_7913, _BlendTexture));
                float2 node_1539 = i.posWorld.rgb.bg;
                float4 node_914 = tex2D(_Rocktexture,TRANSFORM_TEX(node_1539, _Rocktexture));
                float3 diffuseColor = lerp((node_9194.r*node_1839.rgb + node_9194.g*_BlendTexture_var.rgb + node_9194.b*node_914.rgb),_BlendTexture_var.rgb,i.vertexColor.b);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
