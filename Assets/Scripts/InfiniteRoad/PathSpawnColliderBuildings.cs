﻿using UnityEngine;
using System.Collections;

// Original script by Dan adapted by Cameron for functionality with buildings.
public class PathSpawnColliderBuildings : MonoBehaviour
{

    // Public variables.
    public float positionY = 0.81f; // Used to place end of building on a corner.
    public Transform[] PathSpawnPoints; // The PathSpawnPoints array is used to host the locations that the next piece of building and borders will be instantiated.
    public GameObject WholeBuilding; // Cameron - Holds the whole building prefab.
    public GameObject DangerousBorder; // The dangerousborder array holds the end of building on sharp corners. (If having corners).
    private GameObject tempBuilding;

    private GameObject myParent;
    private float spawnZ = 7; // Cameron 
    private bool spawned = false; // Cameron 

    void Start()
    {
        myParent = gameObject.transform.parent.gameObject;
    }

    void Update()
    {
        if (myParent.transform.position.z >= spawnZ && !spawned) // Cameron 
        {
            // Find whether the next path will be straight, left or right
            int randomSpawnPoint = Random.Range(0, PathSpawnPoints.Length);
            for (int i = 0; i < PathSpawnPoints.Length; i++)
            {
                // Instantiate the path, on the set rotation
                if (i == randomSpawnPoint)
                {
                    tempBuilding = Instantiate(WholeBuilding, PathSpawnPoints[i].position, PathSpawnPoints[i].rotation) as GameObject;
                }
                else
                {
                    // Instantiate the border, but rotate it 90 degrees first
                    Vector3 rotation = PathSpawnPoints[i].rotation.eulerAngles;
                    rotation.y += 90;
                    Vector3 position = PathSpawnPoints[i].position;
                    position.y += positionY;
                    Instantiate(DangerousBorder, position, Quaternion.Euler(rotation));
                }
            } 
            spawned = true; // Cameron 
        }
    }

}