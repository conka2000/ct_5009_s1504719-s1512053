using UnityEngine;
using System.Collections;

// This script is responsible for generating the collectables, It begins by declaring some public variables that are set within Unity.

public class ObstacleSpawner : MonoBehaviour
{
    // Holds the location of the collectables or obstacles.
    public Transform[] ObstacleSpawnPoints;
    // Holds the prefab for the obstacles.
    public GameObject[] Obstacles;

    private GameObject tempObject;

	// RandomX 
    public bool RandomX = false;
    public float minX = -2f, maxX = 2f;

    // Use this for initialization
void Start()
    {
        for (int i = 0; i < ObstacleSpawnPoints.Length; i++)
        {
            // Don't instantiate if there's an obstacle
            if (true) 
            {
                CreateObject(ObstacleSpawnPoints[i].position, Obstacles[Random.Range(0, Obstacles.Length)]);
            }
    }
	}


    void CreateObject(Vector3 position, GameObject prefab)
    {
        if (RandomX) // True on the wide path, false on the rotated ones
            position += new Vector3(Random.Range(minX, maxX), 0, 0);

        tempObject=Instantiate(prefab, position, Quaternion.identity) as GameObject;
        tempObject.gameObject.transform.parent = this.gameObject.transform;
    }


}