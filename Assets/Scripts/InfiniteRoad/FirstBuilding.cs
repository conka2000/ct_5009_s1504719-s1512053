﻿using UnityEngine;
using System.Collections;

public class FirstBuilding : MonoBehaviour {

    // This script is made by Cameron
    // This is attached to the first buildings in the scene so that it spawns a line of buildings infront of it.
    // This allows spawning buildings far away

    // Public variables.
    public Transform PathSpawnPoint; // The PathSpawnPoints array is used to host the locations that the next piece of road and borders will be instantiated.
    public GameObject WholeBuilding; // Cameron - Holds the whole building prefab.
    private GameObject tempBuilding;

    void Start()
    {
        // Spawn 4 buildings from a distance to the current building
        for (int i = 1; i <= 4; i++)
        {
            Vector3 newSpawn = new Vector3(0, PathSpawnPoint.position.y, PathSpawnPoint.position.z + (20.887f * i));
            tempBuilding = Instantiate(WholeBuilding, newSpawn, PathSpawnPoint.rotation) as GameObject;
        }
    }

    void Update()
    {
       
    }
}
