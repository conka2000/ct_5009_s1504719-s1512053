﻿using UnityEngine;
using System.Collections;

public class FirstRoad : MonoBehaviour {

    // This script is made by Cameron
    // This is attached to the first roads in the scene so that it spawns a line of roads infront of it.
    // This allows spawning roads far away

    // Public variables.
    public Transform PathSpawnPoint; // The PathSpawnPoints array is used to host the locations that the next piece of road and borders will be instantiated.
    public GameObject Path; // Holds the road prefab.
    public GameObject TutorialJump; // Holds the road prefab.

    void Start()
    {
        //Make the tutorial ramp
        Instantiate(TutorialJump, PathSpawnPoint.position, PathSpawnPoint.rotation);
        // Spawn 4 roads from a distance to the current road
        for (int i = 1; i <= 4; i++)
        {
            Vector3 newSpawn = new Vector3(0, PathSpawnPoint.position.y, PathSpawnPoint.position.z + (20.887f * i));
            Instantiate(Path, newSpawn, PathSpawnPoint.rotation);
        }
    }

    void Update()
    {
     
    }
}
