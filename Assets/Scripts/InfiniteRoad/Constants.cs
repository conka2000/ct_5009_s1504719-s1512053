﻿using UnityEngine;
using System.Collections;

//This class contains static variables (variables which do not need to be visible in the entire project). Apart from the visibility part, this file saves us from hardcoding integers and strings in our game scripts.

public static class Constants
{
	public static readonly string PlayerTag = "Player";
	public static readonly string AnimationStarted = "started";
	public static readonly string AnimationJump = "jump";
	public static readonly string WidePathBorderTag = "WidePathBorder";

	public static readonly string StatusTapToStart = "Tap to start";
	public static readonly string StatusDeadTapToStart = "Dead. Tap to start";
}
