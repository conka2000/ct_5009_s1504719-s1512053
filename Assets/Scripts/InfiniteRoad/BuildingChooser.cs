﻿using UnityEngine;
using System.Collections;

public class BuildingChooser : MonoBehaviour {

    // This script file is made by Cameron
    // This script is responsible for each spawned building choosing which building model to display

    private GameObject currentBuilding;
    // To spawn building
    private GameObject tempBuilding;
    // Buildings to spawn
    public GameObject[] randomBuildings;
    private GameObject firstBuilding;

    // If on left side of road
    public bool leftSide = true;
    private float xLocation;
    private float rotation = 270;

	// Use this for initialization
	void Start()
    { 
        // Building spawn details will differ based on what side of the road the building is on
        if (leftSide)
        {
            xLocation = 13.5f;
            rotation = 270;
        }
        else
        {
            xLocation = -13.5f;
            rotation = 90;
        }
        // Destroy building that is child of this first so this does not display multiple buildings on top of eachother 
        GameObject[] buildings = GameObject.FindGameObjectsWithTag("Building");
        foreach (GameObject building in buildings)
        {
            if (building.transform.IsChildOf(this.gameObject.transform))
            { 
                DestroyObject(building);
            }
        }
        
        // Randomly choose building to spawn
        int randomNumber = Random.Range(0, randomBuildings.Length);
        currentBuilding = randomBuildings[randomNumber];
        // Spawn the building as a child
        tempBuilding = Instantiate(currentBuilding, new Vector3(xLocation, 0, this.transform.position.z), Quaternion.Euler(0, rotation, 0)) as GameObject;
        tempBuilding.transform.parent = gameObject.transform;
        tempBuilding.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        tempBuilding.tag = "Building";
    }
	
}
