﻿// Simple gamestate enumeration that contains three states for the game: Start, Playing and Dead.
// An example would be when the game hasn't started the gameState would be Start.

// Dan Gregg
public enum GameState
{
    Start,
    Playing,
    Dead
}
