﻿using UnityEngine;
using System.Collections;

// Script made by Cameron
// Responsible for letting player touch/click the screen to move the car left or right
public class TouchMove : MonoBehaviour {

    // Whether collider is on the left or right side
    public bool left;

    public GameObject car;
    private CarBehaviour carBehaviour;
	// Use this for initialization
	void Start () {
        carBehaviour = car.GetComponent<CarBehaviour>(); // This is responsible for moving the car
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDrag()
    {
        // When mouse if clicking on collider then set the direction inside the "carBehaviour" script
        if(left)
        {
            carBehaviour.direction = 1; // Left
        }
        else
        {
            carBehaviour.direction = -1; // Right
        }
    }
}
