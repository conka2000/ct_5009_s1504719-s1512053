using UnityEngine;
using System.Collections;

// This script is responsible for generating the collectables & Obstacles, It begins by declaring some public variables that are set within Unity.

// Dan Gregg
public class StuffSpawner : MonoBehaviour
{
    // Holds the location of the collectables or obstacles.
    public Transform[] StuffSpawnPoints;
    // Holds the prefabs for the collectables.
    public GameObject[] Bonus;
    // Holds the prefab for the obstacles.
    public GameObject[] Obstacles;

	// Collectable or Obstacle can be random x position, between set minX and maxX integer.
    public bool RandomX = false;
    public float minX = -2f, maxX = 2f;

    void Start()
    {
        // This generates a percentage that the collectable or obstacle spawns.
        // Random.Range is exclusive for integers but inclusive for floats.
        // http://answers.unity3d.com/questions/585266/question-about-randomrange.html
        bool placeObstacle = Random.Range(0, 2) == 0; // 50% chance to spawn obstacle.
        int obstacleIndex = -1;
        if (placeObstacle)
        {
            // Select a random spawn point, apart from the first one.
            obstacleIndex = Random.Range(1, StuffSpawnPoints.Length);
            CreateObject(StuffSpawnPoints[obstacleIndex].position, Obstacles[Random.Range(0, Obstacles.Length)]);
        }
        
        // This generates a percentage that the collectable spawns.
        for (int i = 0; i < StuffSpawnPoints.Length; i++)
        {
            // Don't instantiate if there's an obstacle.
            if (i == obstacleIndex) continue;
            if (Random.Range(0, 3) == 0) // 33% chances to create collectibles.
            {
                CreateObject(StuffSpawnPoints[i].position, Bonus[Random.Range(0, Bonus.Length)]);
            }
        }
    }

    // Create object function.
    void CreateObject(Vector3 position, GameObject prefab)
    {
        if (RandomX) // True on the wide path, false on the rotated ones
            position += new Vector3(Random.Range(minX, maxX), 0, 0);
        Instantiate(prefab, position, Quaternion.identity);
    }
}