﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

// This script is made by Cameron 
// It is responsible for going to the main menu after hitting an obstacle
public class HitObstacle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision)
    {
        // Look for hitting a gameobject with the "Obstacle" tag and load the main menu scene if so
        if(collision.gameObject.tag == "Obstacle")
        {
            SceneManager.LoadScene(0);
        }
    }
}
