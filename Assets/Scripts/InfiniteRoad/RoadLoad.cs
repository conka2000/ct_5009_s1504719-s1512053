﻿using UnityEngine;
using System.Collections;

// Script made by Cameron
public class RoadLoad : MonoBehaviour {

    public GameObject[] cars; // Cars that can be enabled

    private PersistentObject persistentScript; // Holds car selection from main menu

    // Use this for initialization
    void Start()
    {
        // If there is a PersistentObject then get the number off it and choose a car accordingally
        if (GameObject.Find("PersistentObject") != null)
        {
            persistentScript = GameObject.Find("PersistentObject").GetComponent<PersistentObject>(); // Find the persistent object
            if (persistentScript.carNumber > cars.Length || persistentScript.carNumber < 1)
            {
                cars[0].SetActive(true); // If number is glitched then choose first car
            }
            else
            {
                cars[persistentScript.carNumber - 1].SetActive(true); // If everything ok then choose the car respective to the number in the PersistentObject
            }          
        }
        else
        {
            cars[0].SetActive(true); // Choose first car if no PersistentObject exists
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
