using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Assets.Scripts;

// This script is responsible for moving the road to and past the car

public class CharacterSidewaysMovement : MonoBehaviour
{
    //Cameron - The speed dial holding current speed of car
    public SpeedometerDial dialPivot;

    private CharacterController controller;

    public float Speed = 20.0f;
    //Max gameobject
    
    IInputDetector inputDetector = null;

    // Use this for initialization
    void Start()
    {
        dialPivot = GameObject.Find("Pivot").GetComponent<SpeedometerDial>();
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (GameManager.Instance.GameState)
        {
            case GameState.Start:
                if (Input.GetMouseButtonUp(0))
                {
                    //anim.SetBool(Constants.AnimationStarted, true);
                    var instance = GameManager.Instance;
                    instance.GameState = GameState.Playing;

                    UIManager.Instance.SetStatus(string.Empty);
                }
                break;
            case GameState.Playing:
                UIManager.Instance.IncreaseScore(0.001f * Speed);

                //move the player - Cameron
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + (dialPivot.speed * Time.deltaTime));
                break;
            case GameState.Dead:
                break;
            default:
                break;
        }

    }

    

}