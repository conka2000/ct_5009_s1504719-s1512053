﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Class to display information for the player which is guided by Unity's UI system.
// Singleton class and has some public functions. 

// Dan Gregg
public class UIManager : MonoBehaviour
{
    // Private variable to hold player's score which is modified by the public methods.
    private float score = 0;
    private float tempScore = 0;
    private string url = "highscore.txt";

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    void OnDestroy()
    {
        if (!float.TryParse(System.IO.File.ReadAllText(url), out tempScore))
        {
            tempScore = 0;
        }
        if (score > tempScore)
        {
            System.IO.File.WriteAllText(url, score.ToString());
        }
    }

    // Singleton implementation
    private static UIManager instance;
    public static UIManager Instance
    {
        get
        {
            if (instance == null)
                instance = new UIManager();
            
            return instance;
        }
    }

    protected UIManager()
    {
    }
    
    public void ResetScore()
    {
        score = 0;
        UpdateScoreText();
    }

    public void SetScore(float value)
    {
        score = value;
        UpdateScoreText();
    }

    public void IncreaseScore(float value)
    {
        score += value;
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        ScoreText.text = score.ToString();
    }

    public void SetStatus(string text)
    {
        StatusText.text = text; 
    }


    public Text ScoreText, StatusText;

    

}