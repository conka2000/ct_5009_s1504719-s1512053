﻿using UnityEngine;
using System.Collections;

// To have an infinite road some gameObjects need to be destroyed after the player drives past them, this is to take deallocate space out of the ram and cpu. 
// In otherwords objects behind the car/player no longer need to be 'alive' in the game as it is no longer visible.


// Made by Cameron
public class TimeDestroyer : MonoBehaviour
{
    private int destroyZ = 50; // When object reaches this Z value it will be despawned

    void Start()
    {
    }

    void Update()
    {
        if (gameObject.transform.position.z >= destroyZ)
        {
            Destroy(gameObject); // Destroy when reaches the Z position
        }
    }
    
}



