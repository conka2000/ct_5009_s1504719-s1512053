﻿using UnityEngine;
using System.Collections;

// This script holds basic properties, such as gameState and the player "Die" method.
// GameManager is a singleton which makes only one instance of it 'alive' during the game. This instance is accessible via the static property called Instance.

// Dan Gregg
public class GameManager : MonoBehaviour
{
    public bool CanSwipe { get; set; } // This bool property allows the game to accept swipes from the player (if used on a touch screen & and a level with sharp corners).

    void Start()
    {
    }

    void Awake()
    {
    }

    private static GameManager instance = null;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (new GameObject("GameManager")).AddComponent<GameManager>();
            }
            return instance;
        }
    }

    protected GameManager() // The constructor is declared protected so that the outer classes cannot instantiate a new GameManager, which is necessary for Singleton implementation.
    {
        GameState = GameState.Start;
        CanSwipe = false;
    }

    public GameState GameState { get; set; }
    

    public void Die() // Runs when the car crashes into an object or doesn't land a jump correctly.
    {
        UIManager.Instance.SetStatus(Constants.StatusDeadTapToStart); // UI to show messages related to the crash and death.
        this.GameState = GameState.Dead; // Changes the gameState to Dead.
    }

}
