﻿using UnityEngine;
using System.Collections;
// using Assets.Scripts;

// This class contains the functionality for the collectible items contained within the game.
// Increases the players happiness and allows functionality to compete with others which also includes replayability.

// Dan Gregg
public class Collectables : MonoBehaviour
{
    private string url = "count.txt";
    // Holds the points that this collectable is worth.
    public int ScorePoints = 100;
    // Collectable rotation speed. Can be changed within editor.

    public float rotateSpeed = 50f;
    // Audio component
    private AudioSource audioSource;
    // Holds current coin amount - Cameron
    private int money;

    void Start()
    {
        audioSource = GameObject.FindGameObjectWithTag("PickupSoundBox").GetComponent<AudioSource>(); // Cameron Seeley
    }

    // This function rotates the collectable on the Y axis to make it more visible to the player.
    void Update()
    {
        transform.Rotate(Vector3.left, Time.deltaTime * rotateSpeed);
    }

    // Collision detection for the collectable, deletes upon contact.
    void OnTriggerEnter(Collider col)
    {
        UIManager.Instance.IncreaseScore(ScorePoints);
        audioSource.Play(); // Cameron Seeley
        Destroy(this.gameObject);
        // Read the "count" txt file to get current money, if "count.txt" doesn't exist, reset money to 0 - Cameron
        if (!int.TryParse(System.IO.File.ReadAllText(url), out money))
        {
            money = 0;
        }
        // Add 1 to money - Cameron
        money++;
        // Save coin amount in "count.txt" - Cameron
        System.IO.File.WriteAllText(url, money.ToString());
    }
}