using UnityEngine;
using System.Collections;

// This script is responsible for generating collectables only, It begins by declaring some public variables that are set within Unity.
// This is used for tutorial prefab.

// Dan Gregg
public class CollectableSpawner : MonoBehaviour
{
    // Holds the location of the collectables or obstacles.
    public Transform[] StuffSpawnPoints;
    // Holds the prefabs for the collectables.
    public GameObject[] Bonus;

    private GameObject tempObject;

    // Collectable can be random x position, between set minX and maxX integer.
    public bool RandomX = false;
    public float minX = -2f, maxX = 2f;

    void Start()
    {
        for (int i = 0; i < StuffSpawnPoints.Length; i++)
        {
            if (true) // 100% change to spawn collectable.
            {
                CreateObject(StuffSpawnPoints[i].position, Bonus[Random.Range(0, Bonus.Length)]);
            }
        }
    }

    void CreateObject(Vector3 position, GameObject prefab)
    {
        if (RandomX) // True on the wide path, false on the rotated ones
            position += new Vector3(Random.Range(minX, maxX), 0, 0);
        // Quaternion.Euler allows rotation of the prefab, as to make it be positioned vertically 
        // rather than flat and allows the racer to see the collecatble better.
        tempObject=Instantiate(prefab, position, Quaternion.Euler(new Vector3(0,90,90))) as GameObject;
        tempObject.gameObject.transform.parent = this.gameObject.transform;
    }


}