﻿using UnityEngine;
using System.Collections;

// To add some dificulty to the game it is a good idea to include obstacles.
// Obstacles need to include a triggered rigidbody.

// Dan Gregg
public class Obstacle : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        // If the car hits an obstacle, change the gamestate to Die.
        if (col.gameObject.tag == Constants.PlayerTag)
        {
            GameManager.Instance.Die();
        }
    }
}