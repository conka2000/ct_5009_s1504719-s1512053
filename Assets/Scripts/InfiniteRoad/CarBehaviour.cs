﻿using UnityEngine;
using System.Collections;

public class CarBehaviour : MonoBehaviour {

    // This script is made by Cameron

    [HideInInspector]
    public int direction = 0; // Direction car is moving in -1 is left 1 is right 0 is none
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        // Move object with current direction (direction comes from TouchMove)
        gameObject.transform.position = new Vector3(gameObject.transform.position.x + (direction * 10 * Time.deltaTime), gameObject.transform.position.y, gameObject.transform.position.z);
        // Enforce boundaries
        if (gameObject.transform.position.x >= 4.4 || gameObject.transform.position.x <= -4.4)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x + (-direction * 10 * Time.deltaTime), gameObject.transform.position.y, gameObject.transform.position.z);
        }
        //Direction defaults to 0
        direction = 0;

    }

}
