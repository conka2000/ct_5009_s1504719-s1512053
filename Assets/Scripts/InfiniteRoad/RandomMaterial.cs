﻿using UnityEngine;
using System.Collections;

// This class is used for random material generation.

public class RandomMaterial : MonoBehaviour
{

    void Awake()
    {
        GetComponent<Renderer>().material = GetRandomMaterial();
    }



   // Function to get random colour.
   // During awake it assigns a random colour to the materials.

    public Material GetRandomMaterial()
    {
        int x = Random.Range(0, 5);
        if (x == 0)
            return Resources.Load("Materials/redMaterial") as Material;
        else if (x == 1)
            return Resources.Load("Materials/greenMaterial") as Material;
        else if (x == 2)
            return Resources.Load("Materials/blueMaterial") as Material;
        else if (x == 3)
            return Resources.Load("Materials/yellowMaterial") as Material;
        else if (x == 4)
            return Resources.Load("Materials/purpleMaterial") as Material;
        else
            return Resources.Load("Materials/redMaterial") as Material;
    }

}
