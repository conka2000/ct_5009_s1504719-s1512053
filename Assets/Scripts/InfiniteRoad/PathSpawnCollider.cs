using UnityEngine;
using System.Collections;

// Dan Gregg
public class PathSpawnCollider : MonoBehaviour {

	// Public variables.
    public float positionY = 0.81f; // Used to place end of road on a corner.
    public Transform[] PathSpawnPoints; // The PathSpawnPoints array is used to host the locations that the next piece of road and borders will be instantiated.
    public GameObject[] Paths; // Holds the road prefab.
    public GameObject DangerousBorder; // The dangerousborder array holds the end of road on sharp corners. (If having corners).

    private GameObject myParent;
    private float spawnZ = 7; // Cameron 
    private bool spawned = false; // Cameron 

    void Start()
    {
        myParent = gameObject.transform.parent.gameObject;
    }

    void Update()
    {
        if (myParent.transform.position.z >= spawnZ && !spawned) // If reached a certain Z value - Cameron 
        {
            int randomSpawnPoint = Random.Range(0, PathSpawnPoints.Length);
            for (int i = 0; i < PathSpawnPoints.Length; i++)
            {
                //Vector3 spawnPlace =  new Vector3(PathSpawnPoints[i].position.x, 0, PathSpawnPoints[i].position.z);
                // Instantiate the path, on the set rotation
                int randomPath = Random.Range(0, Paths.Length);
                if (i == randomSpawnPoint)
                    Instantiate(Paths[randomPath], PathSpawnPoints[i].position, PathSpawnPoints[i].rotation);
                else
                {
                    // Instantiate the border, but rotate it 90 degrees first
                    Vector3 rotation = PathSpawnPoints[i].rotation.eulerAngles;
                    rotation.y += 90;
                    Vector3 position = PathSpawnPoints[i].position;
                    position.y += positionY;
                    Instantiate(DangerousBorder, position, Quaternion.Euler(rotation));
                }
            }
            spawned = true;
        }
    }

}