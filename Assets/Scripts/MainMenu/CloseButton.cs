﻿using UnityEngine;
using System.Collections;

// Made by Cameron
// Allows quiting by clicking an X button
public class CloseButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
     
    // Exit game when clicked
    void OnMouseDown()
    {
        Application.Quit();
    }
}
