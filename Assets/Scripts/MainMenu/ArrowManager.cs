﻿using UnityEngine;
using System.Collections;

// Script made by Cameron
// Allows navigation through menu of cars
public class ArrowManager : MonoBehaviour
{
    // Car groups (3 per screen)
    public GameObject cars1to3;
    public GameObject cars4to6;
    public GameObject cars7to9;

    public GameObject leftArrowGO;
    public GameObject rightArrowGO;

    // Check if these are clicked/touched
    private MainMenuArrow leftArrow;
    private MainMenuArrow rightArrow;

    // Which set of cars the screen is showing
    [HideInInspector]
    private int activeGO = 0;
    // Use this for initialization
    void Start()
    {
        // Start with first 3 cars
        cars1to3.SetActive(true);

        leftArrow = leftArrowGO.GetComponent<MainMenuArrow>();
        rightArrow = rightArrowGO.GetComponent<MainMenuArrow>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (activeGO) 
        {
            // If on first 3, move to 4-6 if on right, reset to 7-9 if on left
            case 0:
                if (rightArrow.clicked)
                {
                    cars1to3.SetActive(false);
                    cars4to6.SetActive(true);
                    activeGO = 1;
                    rightArrow.clicked = false;
                } 
                else if (leftArrow.clicked)
                {
                    cars1to3.SetActive(false);
                    cars7to9.SetActive(true);
                    activeGO = 2;
                    leftArrow.clicked = false;
                }
                break;
            // If on 4-6, move to 7-9 if on right, go back to 1-3 if left
            case 1:
                if (rightArrow.clicked)
                {
                    cars4to6.SetActive(false);
                    cars7to9.SetActive(true);
                    activeGO = 2;
                    rightArrow.clicked = false;
                }
                else if (leftArrow.clicked)
                {
                    cars4to6.SetActive(false);
                    cars1to3.SetActive(true);
                    activeGO = 0;
                    leftArrow.clicked = false;
                }
                break;
            // If on 7-9, reset to first 3 if on right, go back to 4-6 if left
            case 2:
                if (rightArrow.clicked)
                {
                    cars7to9.SetActive(false);
                    cars1to3.SetActive(true);
                    activeGO = 0;
                    rightArrow.clicked = false;
                }
                else if (leftArrow.clicked)
                {
                    cars7to9.SetActive(false);
                    cars4to6.SetActive(true);
                    activeGO = 1;
                    leftArrow.clicked = false;
                }
                break;
        }
    }
}
