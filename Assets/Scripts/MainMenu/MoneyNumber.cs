﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Script made by Cameron
// Holds coin number and displays it
public class MoneyNumber : MonoBehaviour {

    private Text uiText;
    public int money = 0;
    // Where coin amount is stored
    private string url = "count.txt";
    // Use this for initialization
    void Start () {
        // Read in current coin amount, if unsuccessful, coin default is 0
        if (!int.TryParse(System.IO.File.ReadAllText(url), out money))
        {
            money = 0;
        }
        // Set text to amount of coins 
        uiText = GetComponent<Text>();
        uiText.text = money.ToString();
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    // Takes money out
    public void ChangeMoney(int amount)
    {
        // Don't allow going into negative coin amounts
        if (amount <= 0 && money <= amount * -1)
        {
            Debug.Log("Too expensive!");
        }
        else
        {
            money += amount;
        }
        // Reset text with new value and save new coin amount
        uiText.text = money.ToString();
        System.IO.File.WriteAllText(url, money.ToString());
    }

}
