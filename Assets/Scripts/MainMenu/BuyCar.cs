﻿using UnityEngine;
using System.Collections;

// Script made by Cameron
// Allows cars to be bought
public class BuyCar : MonoBehaviour {

    public GameObject car;
    private GameObject text;
    private MoneyNumber moneyNumber;
    private BoxCollider carCollider;
    // Txt file where unlocked cars are recorded in
    private string url = "unlocked.txt";
    private string cars = "";
    [Range(1, 9)] // Respective number of the car this is connected to
    public int carNumber;

	// Use this for initialization
	void Start () {
        // Disable collider to choose car
        carCollider = car.GetComponent<BoxCollider>();
        carCollider.enabled = false;

        // Get current money
        text = GameObject.Find("MoneyAmount");
        moneyNumber = text.GetComponent<MoneyNumber>();

        // If car is recorded as unlocked, unlock it automatically
        cars = System.IO.File.ReadAllText(url);
        if(cars.Contains(carNumber.ToString()))
        {
            CarAvailable();
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    // Deduct money and unlock car when clicked
    void OnMouseDown()
    {
        if (moneyNumber.money >= 30) // If 30 coins are available
        {
            moneyNumber.ChangeMoney(-30);        
            System.IO.File.AppendAllText(url, carNumber.ToString()); // Add car onto unlocked list
            CarAvailable();
        }
    }

    // Unlock car
    void CarAvailable()
    {
        carCollider.enabled = true; // Allow it to be chosen
        DestroyImmediate(gameObject); // Destroy coin image
    }
}
