﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Script made by Cameron
// Shows text if buying car
public class MainMenuText : MonoBehaviour {

    private Text myText;

	// Use this for initialization
	void Start ()
    {
        myText = GetComponent<Text>();
        myText.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    public void ChoosingCar()
    {
        myText.text = "Choose a car or buy a new one for 30 coins!";
    }
}
