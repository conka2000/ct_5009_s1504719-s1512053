﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

// Made by Cameron
// Loads car selection screen when clicked/touched
public class PlayButton : MonoBehaviour {
    
    // Gameobjects to unload   
    public GameObject[] oldScreen;

    //Gameobjects to load
    public GameObject carChooser;
    public GameObject arrows;
    private GameObject mainText;
    private MainMenuText mainMenuText;

    // Use this for initialization
    void Start ()
    {
        mainText = GameObject.Find("MainText");
        mainMenuText = mainText.GetComponent<MainMenuText>();
    }
	
	// Update is called once per frame
	void Update ()
    {
       
    }

    void OnMouseDown()
    {
        // Enable gameobjects in car choosing screen
        carChooser.SetActive(true);
        arrows.SetActive(true);

        // Disable gameobjects in last screen
        foreach (GameObject ui in oldScreen)
        {
            ui.SetActive(false);
        }
        // Set text to show choosing car
        mainMenuText.ChoosingCar();
        // Delete the play button
        this.gameObject.SetActive(false);
    }
}
