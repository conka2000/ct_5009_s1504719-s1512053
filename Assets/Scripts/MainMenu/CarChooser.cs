﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

// Script made by Cameron
// Allows car to be chosen and loaded in the next scene
public class CarChooser : MonoBehaviour {
    
    [Range(1,9)]
    public int choiceNumber; // Number associated with car

    private PersistentObject persistentScript; // Where number is stored for next scene

    // Use this for initialization
    void Start () {
        persistentScript = GameObject.Find("PersistentObject").GetComponent<PersistentObject>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnMouseDown()
    {
        // Record cars number and load game
        persistentScript.carNumber = choiceNumber;
        SceneManager.LoadScene(1);
    }
}
