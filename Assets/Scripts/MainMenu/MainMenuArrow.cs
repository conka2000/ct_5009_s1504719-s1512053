﻿using UnityEngine;
using System.Collections;

// Script made by Cameron
// Tells parent if clicked so it can move between car selectors
public class MainMenuArrow : MonoBehaviour {

    [HideInInspector]
    public bool clicked = false;

    void Update()
    {
    }

    void OnMouseDown()
    {
        // Record if clicked
        clicked = true;
    }
    
}
