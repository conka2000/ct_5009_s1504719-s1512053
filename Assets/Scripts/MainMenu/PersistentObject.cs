﻿using UnityEngine;
using System.Collections;

// Made by Cameron
// Responsible for holding the car selection between the transistion
public class PersistentObject : MonoBehaviour {

    [HideInInspector]
    public int carNumber = 1; // Number of car selected, default is 1 (will mean 0 in infinite road scene)

	// Use this for initialization
	void Start () {
        // Set this object to not be destroyed between scenes
        DontDestroyOnLoad(transform.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
