﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Script made by Cameron
// Accelerates the car
public class SpeedometerDial : MonoBehaviour
{
    // Starting speed
    public float speed = 20.0f;

    // Maximum speed
    private float topSpeedVar = 25;

    private float timer = 0.0f;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Until 2 minutes have been reached or top speed has been reached, add onto speed
        if (timer <= 180.0f && speed <= topSpeedVar)
        {
            timer += Time.deltaTime;
            speed += Time.deltaTime * 0.1f;
        }
    }

}